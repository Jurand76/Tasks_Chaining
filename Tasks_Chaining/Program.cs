﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_Chaining
{
    class Program
    {
        static void Main()
        {
            int[] arrayOfIntegers = new int[10];

            Task<int[]> task1 = new Task<int[]>(Arrays.CreateArray10integers);
            Console.WriteLine("Task 1 : ");
            task1.Start();
            arrayOfIntegers = task1.Result;
            Console.WriteLine("Task 1 result : ");
            Arrays.PrintArrayOnConsole(arrayOfIntegers);
            Console.WriteLine();

            Random random = new Random();
            int randomValue = random.Next(-100, 100);

            Task<int[]> task2 = task1.ContinueWith(continued => Arrays.MultiplyArrayByNumber(task1.Result, randomValue));
            arrayOfIntegers = task2.Result;
            Console.WriteLine("Task 2 result (random value = {0}: ", randomValue);
            Arrays.PrintArrayOnConsole(arrayOfIntegers);
            Console.WriteLine();

            Task<int[]> task3 = task2.ContinueWith(continued => Arrays.SortArrayAscending(task2.Result));
            arrayOfIntegers = task3.Result;
            Console.WriteLine("Task 3 result : ");
            Arrays.PrintArrayOnConsole(arrayOfIntegers);
            Console.WriteLine();

            Task<double> task4 = task3.ContinueWith(continued => Arrays.CalculateAverageOfArray(task3.Result));
            double averageOfArray = task4.Result;
            Console.WriteLine("Task 4 result (average): " + averageOfArray);

            Console.WriteLine();
            Console.WriteLine("Press ENTER to exit.");
            Console.ReadLine();
        }
    }
}

