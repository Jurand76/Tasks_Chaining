﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Tasks_Chaining
{
    public class Arrays
    {
        public static void PrintArrayOnConsole(int[] arrayToPrint)
        {
            for (int i = 0; i < arrayToPrint.Length; i++)
            {
                Console.WriteLine(arrayToPrint[i]);
            }
        }

        public static int[] CreateArray10integers()
        {
            Random randInt = new Random();
            int[] array = Enumerable.Repeat(0, 10)
                                    .Select(i => randInt.Next(-1000, 1000)).ToArray();
            return array;
        }

        public static int[] MultiplyArrayByNumber(int[] inputArray, int number)
        {
            if (inputArray.Length == 0 || inputArray is null)
            {
                throw new ArgumentNullException(nameof(inputArray));
            }    
            int[] array = inputArray.Select(i => i * number).ToArray();
            return array;
        }

        public static int[] SortArrayAscending(int[] inputArray)
        {
            if (inputArray.Length == 0 || inputArray is null)
            {
                throw new ArgumentNullException(nameof(inputArray));
            }
            int[] array = inputArray.OrderBy(i => i).ToArray();
            return array;
        }

        public static double CalculateAverageOfArray(int[] inputArray)
        {
            if (inputArray.Length == 0 || inputArray is null)
            {
                throw new ArgumentNullException(nameof(inputArray));
            }
            double result = inputArray.Average();
            return result;
        }
    }
}
